import React from 'react';

import TodoTitle from '../todo-title'
import TodoSearch from '../todo-search'
import TodoList from '../todo-list'

const App = () => {
  return (
    <div className='block_green'>
      <TodoTitle />
      <TodoSearch />
      <TodoList />
    </div>
  )
}

export default App
