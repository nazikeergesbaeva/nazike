import React from 'react';
import ToDoItem from '../todo-item'

const ToDoList = () => {
    return (
      <div>
        <ul>
          <li>
            <ToDoItem action='drink' target='limonade' />
          </li>
          <li>
            <ToDoItem action='make' target='cake' />
          </li>
          <li>
            <ToDoItem action='write' target='poems' />
          </li>
        </ul>
      </div>
    );
  };
  export default ToDoList;